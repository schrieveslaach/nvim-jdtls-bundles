#!/usr/bin/env python3

from argparse import ArgumentParser, BooleanOptionalAction
from io import BytesIO
from pathlib import Path
from urllib.request import urlopen
from zipfile import ZipFile

def download_and_unzip(url, output_directory):
    resp = urlopen(url)
    with ZipFile(BytesIO(resp.read())) as zipfile:
        extract_extensions_from(zipfile, output_directory)

def extract_extensions_from(zipfile, output_directory):
    bundle_files = [name for name in zipfile.namelist() if name.startswith("extension/server")]

    output_directory.mkdir(parents=True, exist_ok=True)

    for bundle_file in bundle_files:
        bs = zipfile.read(bundle_file)

        filename = Path(bundle_file).name
        with open(output_directory / filename, 'wb') as binary_file:
            binary_file.write(bs)

def rm_tree(path):
    if not path.is_dir():
        return

    path = Path(path)
    for child in path.glob('*'):
        if child.is_file():
            child.unlink()
        else:
            rm_tree(child)
    path.rmdir()

if __name__ == '__main__':
    parser = ArgumentParser(description="Install VSCode bundles for eclipes.jdt.ls (a.k.a. jdtls)")
    parser.add_argument("--java-debug", default=True, action=BooleanOptionalAction,
        help="A lightweight Java Debugger based on Java Debug Server which extends the Language Support for Java by Red Hat.")
    parser.add_argument("--java-test", default=True, action=BooleanOptionalAction,
        help="A lightweight extension to run and debug Java test cases.")
    parser.add_argument("--pde", default=False, action=BooleanOptionalAction,
        help=("This extension works as a plugin of Language Support for Java by Red Hat. "
            "It provides the ability to import Eclipse PDE projects and set up the correct target platforms."))

    args = parser.parse_args()

    bundles = Path('bundles')
    rm_tree(bundles)

    if args.java_debug:
        download_and_unzip(
            'https://github.com/microsoft/vscode-java-debug/releases/download/0.40.1/vscjava.vscode-java-debug-0.40.1.vsix',
            bundles / 'java-debug'
        )
    if args.java_test:
        download_and_unzip(
            'https://github.com/microsoft/vscode-java-test/releases/download/0.35.0/vscjava.vscode-java-test-0.35.0.vsix',
            bundles / 'java-test'
        )

    if args.pde:
        with ZipFile('yaozheng.vscode-pde-0.6.0.vsix') as zipfile:
            extract_extensions_from(zipfile, bundles / 'pde')

